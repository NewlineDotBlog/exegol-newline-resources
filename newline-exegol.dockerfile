# Author: The Exegol Project

FROM kalilinux/kali-rolling

ARG TAG="local"
ARG VERSION="local"
ARG BUILD_DATE="n/a"

LABEL org.exegol.tag="${TAG}"
LABEL org.exegol.version="${VERSION}"
LABEL org.exegol.build_date="${BUILD_DATE}"
LABEL org.exegol.app="Exegol"
LABEL org.exegol.src_repository="https://github.com/ShutdownRepo/Exegol-images"

RUN echo "${TAG}-${VERSION}" > /opt/.exegol_version

ADD sources /root/sources
RUN chmod +x /root/sources/install.sh

# Required to ensure the script won't die when running inside Kali
# Ugly fix, I know, but it'll have to do for now
RUN cp /root/sources/install.sh /root/sources/install.sh.bak
RUN sed 's/fapt /fapt-noexit /' /root/sources/install.sh.bak > /root/sources/install.sh

RUN /root/sources/install.sh package_base

# WARNING: package_most_used can't be used with other functions other than: package_base, post_install_clean
# RUN /root/sources/install.sh package_most_used

# WARNING: the following installs (except: package_base, post_install_clean) can't be used with package_most_used
RUN /root/sources/install.sh package_misc
RUN /root/sources/install.sh package_wordlists
RUN /root/sources/install.sh package_cracking
RUN /root/sources/install.sh package_osint
RUN /root/sources/install.sh package_web
RUN /root/sources/install.sh package_c2
RUN /root/sources/install.sh package_ad
#RUN /root/sources/install.sh package_mobile
#RUN /root/sources/install.sh package_iot
#RUN /root/sources/install.sh package_rfid
#RUN /root/sources/install.sh package_voip
#RUN /root/sources/install.sh package_sdr
RUN /root/sources/install.sh package_network
#RUN /root/sources/install.sh package_wifi
#RUN /root/sources/install.sh package_forensic
RUN /root/sources/install.sh package_cloud
#RUN /root/sources/install.sh package_steganography
#RUN /root/sources/install.sh package_reverse
RUN /root/sources/install.sh package_crypto
RUN /root/sources/install.sh package_code_analysis

### Newline modifications

RUN wget https://gitlab.com/NewlineDotBlog/exegol-newline-resources/-/raw/main/newline-exegol-tools.sh -O /root/sources/newline-exegol-tools.sh && chmod +x /root/sources/newline-exegol-tools.sh && /root/sources/newline-exegol-tools.sh install_all

### ---

RUN /root/sources/install.sh post_install_clean

RUN rm -rf /root/sources

WORKDIR /workspace

ENTRYPOINT ["/.exegol/entrypoint.sh"]
