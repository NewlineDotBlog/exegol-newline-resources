# exegol-newline-resources

Small repository with a few custom [Exegol](https://github.com/ThePorgs/Exegol) resources.

## Quick setup

Simply run the following command to get completely set up with the Newline exegol docker image. This may take a while to build.

```
wget https://gitlab.com/NewlineDotBlog/exegol-newline-resources/-/raw/main/newline-exegol.dockerfile -O ~/.local/exegol-docker-build/newline-exegol.dockerfile && exegol install newline-exegol.dockerfile newline-exegol 
```

## newline-exegol.dockerfile

Place this file in your `exegol-docker-build` (usually found at `~/.local/exegol-docker-build`). Then, you should be simply able to build it with `exegol install newline-exegol.dockerfile newline-exegol`.

## newline-exegol-tools.sh

This file contains a few additional installable tools for use in exegol. You can use this file seperately from your own custom exegol images.
