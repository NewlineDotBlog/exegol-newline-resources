# Exegol tool installer script. Use with './newline-exegol-tools.sh install_all' to install all tools 

# This installer should work for any ubuntu based distro and should also work on the ubuntu based exegol images.
install_zaproxy() {
  mkdir /tmp/zap-install
  cd /tmp/zap-install
  apt-get install -q -y --fix-missing rsync wget curl openjdk-11-jdk xmlstarlet unzip git && apt-get clean
wget -qO- https://raw.githubusercontent.com/zaproxy/zap-admin/master/ZapVersions.xml | xmlstarlet sel -t -v //url |grep -i Linux | wget --content-disposition -i - -O - | tar zxv
  mv ZAP*/* .
  rm -R ZAP*
  chmod +x zap.sh
  ./zap.sh -cmd -silent -addonupdate
  mkdir /opt/zaproxy
  rsync -av /tmp/zap-install/ /opt/zaproxy/
  ln -s /opt/zaproxy/zap.sh /usr/local/bin/zap.sh
  ln -s /opt/zaproxy/zap.sh /usr/local/bin/zaproxy
  rm -rf /tmp/zap-install
  echo 'Completed installation of zaproxy'
}

install_useful_aliases() {
  echo 'alias smbserver="python3 /usr/share/doc/python3-impacket/examples/smbserver.py kali . -smb2support"' >> ~/.bashrc
  echo 'alias httpserver="sudo python3 -m http.server 80"' >> ~/.bashrc
  echo 'alias clip="xclip -sel clip"'  >> ~/.bashrc
  echo 'alias nse-find="cat /usr/share/nmap/scripts/script.db | grep "' >> ~/.bashrc
  echo 'Installed useful aliases'
}

# This will install seclists in the wordlists directory. 
# This is excluded from install_all as it's recommended to instead install seclists manually 
# in /my-resources so it doesn't have to take up unnecesary space in your images.
install_seclists() {
  apt-get install git
	cd /usr/share/wordlists
  git clone https://github.com/danielmiessler/SecLists.git
  echo 'Completed seclists install'
}

# This will allow the user to setup a custom 'run-once' script to set up symlinks to their resources for example or other
# container specific modifications that should be done in container.
install_resouce_script() {
  echo '' >> ~/.bashrc
  echo 'if [ ! -f ~/.resource_script_skip ]; then'  >> ~/.bashrc
  echo '  echo "Running first time resource script"' >> ~/.bashrc
  echo '  /my-resources/resource_script.sh' >> ~/.bashrc
  echo 'fi' >> ~/.bashrc
  echo 'touch ~/.resource_script_skip' >> ~/.bashrc
  echo 'Installed resource script trigger'
}

install_all() {
    install_zaproxy
    install_useful_aliases
    install_resouce_script
}

# Entry point for the installation
if [[ $EUID -ne 0 ]]; then
  echo -e "${RED}"
  echo "You must be a root user" 2>&1
  echo -e "${NOCOLOR}"
  exit 1
else
  if declare -f "$1" > /dev/null
  echo 'Running apt-get update for all future tool installs'
  export DEBIAN_FRONTEND=noninteractive
  apt-get update
  then
    if [[ -f '/.dockerenv' ]]; then
      echo -e "${GREEN}"
      echo "This script is running in docker, as it should :)"
      echo "If you see things in red, don't panic, it's usually not errors, just badly handled colors"
      echo -e "${NOCOLOR}${BLUE}"
      echo -e "${NOCOLOR}"
      sleep 2
      "$@"
    else
      echo -e "${RED}"
      echo "[!] Careful : this script is supposed to be run inside a docker/VM, do not run this on your host unless you know what you are doing and have done backups. You are warned :)"
      echo "[*] Sleeping 30 seconds, just in case... You can still stop this"
      echo -e "${NOCOLOR}"
      #sleep 1
      "$@"
    fi
  else
    echo "'$1' is not a known function name" >&2
    exit 1
  fi
fi
